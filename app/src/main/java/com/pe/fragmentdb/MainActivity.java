package com.pe.fragmentdb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button butFragment;
    Button butBD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        butFragment = findViewById(R.id.butFragment);
        butBD = findViewById(R.id.butBD);


        butFragment.setOnClickListener(this);
        butBD.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.butFragment) {
            Intent intent = new Intent(this, FragmentActivity.class);
            startActivity(intent);

        } else if (view.getId() == R.id.butBD) {
            Intent intent = new Intent(this,FormActivity.class);
            startActivity(intent);

        }


    }
}
