package com.pe.fragmentdb;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

public class FormActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private EditText eteUsuario;
    private CheckBox chkRecordar;

    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        eteUsuario = findViewById(R.id.eteUsuario);
        chkRecordar = findViewById(R.id.chkRecordar);

        chkRecordar.setOnCheckedChangeListener(this);

        sp = getSharedPreferences("ejemploSP", MODE_PRIVATE);


        String spUsuario = sp.getString("llaveUsuario", "");

        if (!spUsuario.equals("")) {
            eteUsuario.setText(spUsuario);
            chkRecordar.setChecked(true);
        }


    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SharedPreferences.Editor editor = sp.edit();
        if (isChecked) {
            editor.putString("llaveUsuario", eteUsuario.getText().toString());
            editor.apply();

        } else {
            editor.remove("llaveUsuario");
            editor.apply();
        }


    }
}
