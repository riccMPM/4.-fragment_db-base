package com.pe.fragmentdb;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class FragmentActivity extends AppCompatActivity implements View.OnClickListener, PrimerFragment.OnPrimerFragmentListener, SegundoFragment.OnSegundoFragmentListener {

    Button butUno, butDos;
    FrameLayout fmeFragment;
    TextView tviTexto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        butUno = findViewById(R.id.butUno);
        butDos = findViewById(R.id.butDos);
        fmeFragment = findViewById(R.id.fmeFragment);
        tviTexto = findViewById(R.id.tviTexto);

        butUno.setOnClickListener(this);
        butDos.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.butUno:
                loadFragment(new PrimerFragment());
                break;
            case R.id.butDos:
                loadFragment(new SegundoFragment());
                break;
        }
    }

    private void loadFragment(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.fmeFragment, fragment);
        fragmentTransaction.commit(); // save the changes
    }


    @Override
    public void onBotonSeleccionado(String mensaje) {
        tviTexto.setText(mensaje);
    }

    @Override
    public void onSegundoBotonSeleccionado(String mensaje) {

    }
}