package com.pe.fragmentdb;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Riccardomp on 24/03/18.
 */

public class SegundoFragment extends Fragment implements View.OnClickListener {

    Button butSegundo;
    OnSegundoFragmentListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_segundo, container, false);


        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        butSegundo = view.findViewById(R.id.butSegundo);

        butSegundo.setOnClickListener(this);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Utilizado para instanciar interface
        try {
            listener = (OnSegundoFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public void onClick(View v) {
        Toast.makeText(getActivity(), "Segundo Fragment", Toast.LENGTH_SHORT).show();

        listener.onSegundoBotonSeleccionado("Segundo Fragment click!");
    }

    // Container Activity must implement this interface
    public interface OnSegundoFragmentListener {
        public void onSegundoBotonSeleccionado(String mensaje);
    }
}