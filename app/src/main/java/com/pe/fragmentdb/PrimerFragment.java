package com.pe.fragmentdb;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class PrimerFragment extends Fragment implements View.OnClickListener {

    Button butPrimerFragment;
    OnPrimerFragmentListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_primero, container, false);


        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        butPrimerFragment = view.findViewById(R.id.butPrimerFragment);

        butPrimerFragment.setOnClickListener(this);


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Utilizado para instanciar interface
        try {
            listener = (OnPrimerFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onClick(View v) {

        listener.onBotonSeleccionado("Boton primer fragment seleccionado!");

    }


    // Container Activity must implement this interface
    public interface OnPrimerFragmentListener {
        public void onBotonSeleccionado(String mensaje);
    }
}
